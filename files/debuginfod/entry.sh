#!/bin/bash

[ -d /var/lib/debug-images ] || mkdir -p /var/lib/debug-images
exec debuginfod -Z tar=cat /var/lib/debug-images
