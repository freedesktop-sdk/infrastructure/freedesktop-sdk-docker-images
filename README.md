
## Overview

Docker images needed to run the freedesktop-sdk CI

Images are created for amd64, aarch64 and ppc64le architectures.

## Image digests

To derive the image that you require, the digest of the image generated is the sha of the git commit that the CI was triggered on.

## Using the Image with Toolbox

### Dependencies
- Toolbox (https://github.com/containers/toolbox)

### Usage
```bash
toolbox create ContainerName -i Image URL
toolbox enter ContainerName
```

### Example
```bash
toolbox create freedesktop -i registry.gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/bst2:fc483978231b6ee538a37661a2d9cbc8f17b6753
toolbox enter freedesktop
```


The required images can be found at the following link:
https://gitlab.com/freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images/container_registry

## image corresponding to commit

```
commit 97f80d4d4e7aeb16a746e838c38417500f25e61b
Merge: 271747b 6258807
Author: Javier Jardón <jjardon@gnome.org>
Date:   Wed May 13 19:34:39 2020 +0000

    Merge branch 'jjardon/bst_1_4_3' into 'master'

    Use buildstream 1.4.3

    See merge request freedesktop-sdk/infrastructure/freedesktop-sdk-docker-images!110
```